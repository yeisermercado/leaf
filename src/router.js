import Vue from 'vue'
import Router from 'vue-router'
// import Home from './views/Home.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/accountsetting',
      name: 'accountsetting',
      component: () => import(/* webpackChunkName: "Home" */ './views/account/AccountSetting.vue'),
      children: [
        { path: 'editperfil', 
          component: () => import(/* webpackChunkName: "Home" */ './views/account/editPerfil.vue')
        },
        { path: 'changepassword', 
          component: () => import(/* webpackChunkName: "Home" */ './views/account/changePassword.vue')
        }
      ]
    },
    {
      path: '/profile',
      name: 'profile',
      component: () => import(/* webpackChunkName: "Home" */ './views/account/profile.vue')
    },
    {
      path: '/home',
      name: 'home',
      component: () => import(/* webpackChunkName: "Home" */ './views/Home.vue')
    },
    {
      path: '/cartelera',
      name: 'cartelera',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/cartelera-empresarial/cartelera.vue')
    },
    {
      path: '/miembros',
      name: 'miembros',
      component: () => import(/* webpackChunkName: "about" */ './views/miembros/miembros.vue')
    },
    {
      path: '/perfiles-cargos',
      name: 'perfiles-cargos',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/perfiles-cargos/perfiles-cargos.vue')
    },
    {
      path: '/auditoria',
      name: 'auditoria',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/auditoria/auditoria.vue')
    },
    {
      path: '/login',
      name: 'login',
      component: () => import(/* webpackChunkName: "login" */ './components/login/Login.vue')
    },
    {
      path: '/recover',
      name: 'recover',
      component: () => import(/* webpackChunkName: "recover" */ './components/recover/recover.vue')
    },
    {
      path: '*',
      redirect: '/login'
    },
    {
      path: '/accountactive',
      name: 'accountactive',
      component: () => import(/* webpackChunkName: "accountactive" */ './components/login/accountactive.vue')
    },
    {
      path: '/resetpassword',
      name: 'resetpassword',
      component: () => import(/* webpackChunkName: "ResetPassword" */ './components/recover/ResetPassword.vue')
    },
    {
      path: '/activarinvitado',
      name: 'activarinvitado',
      component: () => import(/* webpackChunkName: "ResetPassword" */ './components/invitacion/mensajeinvitacion.vue')
    },
    {
      path: '/editEmpresa',
      name: 'editEmpresa',
      component: () => import(/* webpackChunkName: "Home" */ './views/account/editEmpresa.vue')
    },
  ]
})
