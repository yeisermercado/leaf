import axios from 'axios';

export const HTTP = axios.create({
  // baseURL: `http://190.144.2.250:5000/`,
  baseURL: `http://192.168.1.100:5000/`,
  headers: {
    Authorization: 'Bearer '+localStorage.getItem("Token")
  }
})